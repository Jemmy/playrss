package controllers

import play.api.mvc._
import scala.concurrent._
import models.News
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import models.parsers.Parser
import com.mongodb.casbah.Imports._

object NewsController extends Controller {

  /**
   * Get news JSON
   * @param tag optional tag filter
   * @param pubDate optional pubDate filter for loading news before this UNIX timestamp
   * @return
   */
  def news(tag: String, pubDate: Int) = Action.async {
    val futureNews = Future {
      try {
        News asJson News.allNews(tag, pubDate)
      } catch {
        case e: MongoException => throw e
      }
    }

    futureNews.map {
      news => Ok(news).as("application/json")
    }.recover {
      case e: MongoException => InternalServerError("{error: 'DB Error: " + e.getMessage + "'}").as("application/json")
    }
  }

  /**
   * Start new RSS parsing and return first N news
   * @return
   */
  def parseRSS = Action.async {
    val futureParse = scala.concurrent.Future {
      try {
        Parser.downloadItems(News.addNews(_))
        News asJson News.allNews()
      } catch {
        case e: Exception => throw e
      }
    }

    futureParse.map(newsJson => Ok(newsJson).as("application/json")).recover {
      case e: MongoException => InternalServerError("{error: 'DB Error: " + e.getMessage + "'}").as("application/json")
      case e: Exception => InternalServerError("{error: 'Parse Error: " + e.getMessage + "'}").as("application/json")
    }

  }

}
