package models.parsers

import scala.xml._
import models.News
import java.util.Locale
import java.text.{SimpleDateFormat, ParseException}
import java.text._
import play.api.Play
import collection.JavaConversions._

/**
 * Simple XML parser
 */
object Parser {

  /**
   * RSS urls from application.conf
   */
  val urls = try {
    Play.current.configuration.getStringList("rss.urls").map(_.toList).getOrElse(List())
  } catch {
    case e: Throwable => List()
  }

  /**
   * Download and parse XML, fill News object and pass it to callback
   * @param cb
   */
  def downloadItems(cb: (News) => Unit) = {
    urls.foreach {
      (url: String) =>
        try {
          parseItem(XML.load(url)).foreach(cb(_))
        } catch {
          case e: Exception => throw e
        }
    }
  }

  /**
   * Parse standart RSS time
   * @param s
   * @return
   */
  def parseDateTime(s: String): Long = {
    try {
      new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH).parse(s).getTime / 1000
    } catch {
      case e: ParseException => 0
    }
  }

  /**
   * For all items in RSS parse its content and return list of News objects
   * @param xml
   * @return
   */
  def parseItem(xml: Elem): List[News] = (xml \\ "item").map(buildNews(_)).toList

  /**
   * Fill and return News object
   * @param node
   * @return
   */
  def buildNews(node: Node) = new News(
    title = (node \\ "title").text,
    link = (node \\ "link").text,
    content = (node \\ "description").text,
    pubDate = parseDateTime((node \\ "pubDate").text),
    tags = Seq((node \\ "category").text))

}