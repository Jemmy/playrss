package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json

/**
 * Default news container
 * @param id MongoID
 * @param title
 * @param link
 * @param content
 * @param tags Sequence of tags. Since categories could be joined into one
 * @param pubDate
 */
case class News(val id: String = "0", val title: String, val link: String, val content: String, val tags: Seq[String], val pubDate: Long)

/**
 * News object allows to operate with news in database. Companion object for News class
 */
object News {

  /**
   * Default news limit for DB select
   */
  val newsLimit = 10

  /**
   * News collection
   */
  private val col: MongoCollection = Database.collection("news")

  /**
   * Method to add news to database
   * @param news filled News object
   * @return
   */
  def addNews(news: News) = {
    val toInsert = MongoDBObject("title" -> news.title, "content" -> news.content, "link" -> news.link, "tags" -> news.tags, "pubDate" -> news.pubDate)
    try {
      col.insert(toInsert)
    } catch {
      case e: Exception =>
    }
  }

  /**
   * Get first N news.
   * @param tag Tag to match news. Could be an empty string
   * @param pubDate UNIX timestamp. If parameter is set, method will return N news before pubDate
   * @return
   */
  def allNews(tag: String = "", pubDate: Int = (System.currentTimeMillis() / 1000).toInt): Array[News] = {
    try {
      val filter = if (tag.length == 0) {
        MongoDBObject("pubDate" -> MongoDBObject("$lt" -> pubDate))
      } else MongoDBObject("tags" -> tag, "pubDate" -> MongoDBObject("$lt" -> pubDate))

      getNews(filter)

    } catch {
      case e: MongoException => throw e
    }
  }

  /**
   * Get news from DB
   * @param filter filter for find() method
   * @param sort object for sorting. by default sorts by pubDate
   * @param limit limit for news select. by default equals to newsLimit
   * @return
   */
  def getNews(filter: MongoDBObject, sort: MongoDBObject = MongoDBObject("pubDate" -> -1), limit: Int = newsLimit): Array[News] = {
    try {
      col.find(filter).
        sort(sort).
        limit(limit).
        map((o: DBObject) => {
        new News(
          id = o.as[ObjectId]("_id").toString,
          title = o.as[String]("title"),
          link = o.as[String]("link"),
          content = o.as[String]("content"),
          tags = o.as[MongoDBList]("tags").map(_.toString),
          pubDate = o.as[Long]("pubDate"))
      }).toArray
    } catch {
      case e: MongoException => throw e
    }
  }

  /**
   * Play Magic
   * @return
   */
  implicit def newsWrites = Json.writes[News]

  /**
   * Converts array of news to json
   * @param src Array of News instances
   * @return JSON string
   */
  def asJson(src: Array[News]) = {
    Json.stringify(Json.toJson(src))
  }

}