define ["app/NewsModule","url"], (newsModule,urlParser)->
  newsModule.controller "NewsCtrl", ["$scope", "$http", "broadcastService", ($scope, $http, broadcastService)->

    $scope.formatDate = (dateObj)->
      dateObj = new Date(dateObj * 1000)
      h = if dateObj.getHours().toString().length == 1 then "0" + dateObj.getHours() else dateObj.getHours()
      min = if dateObj.getMinutes().toString().length == 1 then "0" + dateObj.getMinutes() else dateObj.getMinutes()
      sec = if dateObj.getSeconds().toString().length == 1 then "0" + dateObj.getSeconds() else dateObj.getSeconds()
      day = if dateObj.getDate().toString().length == 1 then "0" + dateObj.getDate() else dateObj.getDate()
      month = if (dateObj.getMonth()+1).toString().length == 1 then "0" + (dateObj.getMonth() + 1) else dateObj.getMonth() + 1
      year = dateObj.getFullYear()
      "#{h}:#{min}:#{sec} #{day}.#{month}.#{year}"

    $scope.tag = null
    $scope.busy = false
    $scope.news = news = []
    $scope.after = 0

    buildUrl = ()->
      params = []
      if($scope.after > 0) then params.push("pubDate=#{$scope.after}")
      if($scope.tag) then params.push("tag=#{$scope.tag}")
      "/news?"+params.join("&")

    $scope.parseUrl = (u)->
      urlParser.parse(u).host

    $scope.more = ()->
      if $scope.busy then return
      $scope.busy = true

      url = buildUrl()

      $http.get(url).success (data)->
        if(data.length)
          Array::push.apply $scope.news, data
          $scope.after = data.slice(-1)[0].pubDate
          $scope.busy = false
        else
          #error

    $scope.loadByTag = (tag)->
      $scope.busy = true
      $scope.tag = tag

      url = buildUrl()

      $http.get(url).success (data)->
        if(data.length)
          broadcastService.broadcast("selectTag",$scope.tag)
          $scope.news = data
          $scope.after = data.slice(-1)[0].pubDate
          $scope.busy = false
        else
          #error

    $scope.$on "loadByTag", ()->
      $scope.after = 0
      $scope.loadByTag(broadcastService.message["loadByTag"])

    $scope.$on "loadAll", ()->
      $scope.after = 0
      $scope.tag = false
      $scope.busy = false
      $scope.loadByTag()

    $scope.$on "newParse", ()->
      $http.get("/parse").success (data)->
        if(data.length)
          $scope.news = data
          $scope.after = data.slice(-1)[0].pubDate
          $scope.busy = false
        else
          #error
        broadcastService.broadcast "parseDone", 1
  ]